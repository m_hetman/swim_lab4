package com.example.simpleplayer

import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import kotlinx.android.synthetic.main.player_fragment.*

class PlayerFragment: Fragment()
{
    companion object {
        private var initiated = false
        private lateinit var runnable: Runnable
        private lateinit var handler: Handler
        private lateinit var player: MediaPlayer
        var isIconPlay = true
        private var pos = 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.player_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        super.onCreate(savedInstanceState)
        if(!initiated){
            handler = Handler()
            bindTrack()
            initMediaPlayer()
        }
        setTrackInfo()
        initSeekBar()
        seekBar.max = player.duration
        playStop.setOnClickListener {
            setTrackState()
            changeSeekBar()
            isIconPlay = !isIconPlay
        }
        nextPrevListener()
        setCurrPlayIcon()
        initiated = true
    }

    override fun onResume() {
        super.onResume()
        changeSeekBar()
    }

    private fun initMediaPlayer(){
        if(player.isPlaying)
            playStop.setImageDrawable(activity!!.getDrawable(R.drawable.stop_button))
        player.setOnPreparedListener {
            changeSeekBar()
        }
    }

    private fun changeSeekBar() {
        seekBar?.progress = player.currentPosition
        if(player.isPlaying){
            runnable = Runnable { changeSeekBar() }
            handler.postDelayed(runnable,1000)
        }
    }


    private fun initSeekBar(){
        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser){
                    player.seekTo(progress)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })
    }

    private fun setTrackState(){
        if(!player.isPlaying) {
            player.start()
            playStop.setImageDrawable(activity!!.getDrawable(R.drawable.stop_button))
        } else {
            player.pause()
            playStop.setImageDrawable(activity!!.getDrawable(R.drawable.play_button))
        }
    }

    private fun bindTrack(){
        player = MediaPlayer.create(activity!!, Data.SongIds[pos])
        miniCover.setImageDrawable(activity!!.getDrawable(Data.CoverIds[pos]))
        artistName.text = Data.Artists[pos]
        trackTitle.text = Data.Titles[pos]
    }

    private fun nextPrevListener() {
        next.setOnClickListener{
            if(pos < Data.SongIds.size - 1){
                restartPlayer(pos+1)
            }
        }
        prev.setOnClickListener{
            if(0 < pos) {
                restartPlayer(pos-1)
            }
        }
    }

    private fun setTrackInfo(){
        miniCover.setImageDrawable(activity!!.getDrawable(Data.CoverIds[pos]))
        artistName.text = Data.Artists[pos]
        trackTitle.text = Data.Titles[pos]
    }

    private fun setCurrPlayIcon(){
        if(!isIconPlay) {
            playStop.setImageDrawable(activity!!.getDrawable(R.drawable.stop_button))
        } else {
            playStop.setImageDrawable(activity!!.getDrawable(R.drawable.play_button))
        }
    }

    fun restartPlayer(newPos : Int) {
        pos = newPos
        player.stop()
        bindTrack()
        initMediaPlayer()
        setTrackState()
        player.start()
        changeSeekBar()
    }


}