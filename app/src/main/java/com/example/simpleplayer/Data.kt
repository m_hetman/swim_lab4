package com.example.simpleplayer

object Data {
    val Titles = arrayOf("Guardian Angel", "Pomarańcza", "Enter Sandman")
    val Artists = arrayOf("Riverside", "Golec uOrkiestra", "Metallica")
    val CoverIds = arrayOf(R.drawable.wasteland, R.drawable.golec, R.drawable.blackalbum)
    val SongIds = arrayOf(R.raw.guardianangel, R.raw.pomarancza, R.raw.entersandman)
}