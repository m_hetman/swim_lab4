package com.example.simpleplayer

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class ListAdapter(val listener : OnItemClickedListener) : RecyclerView.Adapter<ListAdapter.ListViewHolder>() {
    interface OnItemClickedListener{
        fun onItemClicked()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ListViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.song_item, p0, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
       return Data.SongIds.size
    }

    override fun onBindViewHolder(p0: ListViewHolder, p1: Int) {
        p0.bindView(p1)
    }

    inner class ListViewHolder(itemView : View): RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val mTrackNameText = itemView.findViewById<TextView>(R.id.songItemTitle)!!
        private val mArtistName = itemView.findViewById<TextView>(R.id.songItemArtist)!!
        private val mAlbumCover = itemView.findViewById<ImageView>(R.id.songItemAlbumCover)!!
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            (listener as MainActivity).currPos = this.adapterPosition
            listener.onItemClicked()
        }

        fun bindView(position : Int) {
            mTrackNameText.text = Data.Titles[position]
            mArtistName.text = Data.Artists[position]
            mAlbumCover.setImageResource(Data.CoverIds[position])
        }
    }
}