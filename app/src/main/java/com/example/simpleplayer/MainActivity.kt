package com.example.simpleplayer

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), ListAdapter.OnItemClickedListener {

    lateinit var pager: ViewPager
    lateinit var pagerAdapter: PagerAdapter
    var currPos = 0
    override fun onItemClicked() {
        pagerAdapter.onItemSelected(currPos)
        PlayerFragment.isIconPlay = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupPager()
        navigation.setViewPager(findViewById(R.id.pager), 0)
    }

    private fun setupPager(){
        pager = findViewById(R.id.pager)
        pagerAdapter = PagerAdapter(supportFragmentManager)
        pager.adapter = pagerAdapter
    }
}
