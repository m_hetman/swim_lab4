package com.example.simpleplayer

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter


class PagerAdapter(fm : FragmentManager) : FragmentPagerAdapter(fm) {
    private val pf = PlayerFragment()
    override fun getCount(): Int {
        return 2
    }

    override fun getItem(p0: Int): Fragment {
        return if(p0 == 0) SongsListFragment()
        else pf
    }

    fun onItemSelected(pos : Int) {
        pf.restartPlayer(pos)
    }
}