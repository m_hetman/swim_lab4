package com.example.simpleplayer

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


class SongsListFragment : Fragment() {

    private lateinit var callback : ListAdapter.OnItemClickedListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.songs_list_fragment, container, false)
        val rv = view.findViewById<RecyclerView>(R.id.songs)
        val listAdapter = ListAdapter(callback)
        rv.adapter = listAdapter
        val layoutManager = LinearLayoutManager(activity)
        rv.layoutManager = layoutManager

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is ListAdapter.OnItemClickedListener) {
            callback = context
        }
    }
}